/*
 * Copyright (C) 2024 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.federatedcompute.services.training.util;

import android.annotation.Nullable;
import android.federatedcompute.aidl.IExampleStoreIterator;

import com.android.ondevicepersonalization.internal.util.DataClass;

@DataClass(genBuilder = true, genEqualsHashCode = true)
public class EligibilityResult {
    private boolean mEligible = true;
    @Nullable private IExampleStoreIterator mExampleStoreIterator;

    // Code below generated by codegen v1.0.23.
    //
    // DO NOT MODIFY!
    // CHECKSTYLE:OFF Generated code
    //
    // To regenerate run:
    // $ codegen
    // $ANDROID_BUILD_TOP/packages/modules/OnDevicePersonalization/federatedcompute/src/com/android/federatedcompute/services/training/util/EligibilityResult.java
    //
    // To exclude the generated code from IntelliJ auto-formatting enable (one-time):
    //   Settings > Editor > Code Style > Formatter Control
    // @formatter:off

    @DataClass.Generated.Member
    /* package-private */ EligibilityResult(
            boolean eligible, @Nullable IExampleStoreIterator exampleStoreIterator) {
        this.mEligible = eligible;
        this.mExampleStoreIterator = exampleStoreIterator;

        // onConstructed(); // You can define this method to get a callback
    }

    @DataClass.Generated.Member
    public boolean isEligible() {
        return mEligible;
    }

    @DataClass.Generated.Member
    public @Nullable IExampleStoreIterator getExampleStoreIterator() {
        return mExampleStoreIterator;
    }

    @Override
    @DataClass.Generated.Member
    public boolean equals(@Nullable Object o) {
        // You can override field equality logic by defining either of the methods like:
        // boolean fieldNameEquals(EligibilityResult other) { ... }
        // boolean fieldNameEquals(FieldType otherValue) { ... }

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        @SuppressWarnings("unchecked")
        EligibilityResult that = (EligibilityResult) o;
        //noinspection PointlessBooleanExpression
        return true
                && mEligible == that.mEligible
                && java.util.Objects.equals(mExampleStoreIterator, that.mExampleStoreIterator);
    }

    @Override
    @DataClass.Generated.Member
    public int hashCode() {
        // You can override field hashCode logic by defining methods like:
        // int fieldNameHashCode() { ... }

        int _hash = 1;
        _hash = 31 * _hash + Boolean.hashCode(mEligible);
        _hash = 31 * _hash + java.util.Objects.hashCode(mExampleStoreIterator);
        return _hash;
    }

    /** A builder for {@link EligibilityResult} */
    @SuppressWarnings("WeakerAccess")
    @DataClass.Generated.Member
    public static class Builder {

        private boolean mEligible;
        private @Nullable IExampleStoreIterator mExampleStoreIterator;

        private long mBuilderFieldsSet = 0L;

        public Builder() {}

        @DataClass.Generated.Member
        public @android.annotation.NonNull Builder setEligible(boolean value) {
            checkNotUsed();
            mBuilderFieldsSet |= 0x1;
            mEligible = value;
            return this;
        }

        @DataClass.Generated.Member
        public @android.annotation.NonNull Builder setExampleStoreIterator(
                @android.annotation.NonNull IExampleStoreIterator value) {
            checkNotUsed();
            mBuilderFieldsSet |= 0x2;
            mExampleStoreIterator = value;
            return this;
        }

        /** Builds the instance. This builder should not be touched after calling this! */
        public @android.annotation.NonNull EligibilityResult build() {
            checkNotUsed();
            mBuilderFieldsSet |= 0x4; // Mark builder used

            if ((mBuilderFieldsSet & 0x1) == 0) {
                mEligible = true;
            }
            EligibilityResult o = new EligibilityResult(mEligible, mExampleStoreIterator);
            return o;
        }

        private void checkNotUsed() {
            if ((mBuilderFieldsSet & 0x4) != 0) {
                throw new IllegalStateException(
                        "This Builder should not be reused. Use a new Builder instance instead");
            }
        }
    }

    // @formatter:on
    // End of generated code

}
